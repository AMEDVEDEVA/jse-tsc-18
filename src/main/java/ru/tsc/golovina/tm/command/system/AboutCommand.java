package ru.tsc.golovina.tm.command.system;

import ru.tsc.golovina.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println("Developer: Alla Golovina");
        System.out.println("e-mail: amedvedeva@tsconsulting.com");
    }

}
